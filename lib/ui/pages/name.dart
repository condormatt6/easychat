import 'package:easychat/bloc/name/bloc.dart';
import 'package:easychat/repository/userRepository.dart';
import 'package:easychat/ui/widgets/nameForm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Name extends StatelessWidget {
  final _userRepository;
  final userId;

  Name({@required UserRepository userRepository, String userId})
      : assert(userRepository != null && userId != null),
        _userRepository = userRepository,
        userId = userId;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   title: Text("Profile Setup"),
        //   centerTitle: true,
        //   backgroundColor: backgroundColor,
        //   elevation: 0,
        // ),
        body: BlocProvider<NameBloc>(
      create: (context) => NameBloc(userRepository: _userRepository),
      child: NameForm(userRepository: _userRepository),
    ));
  }
}
