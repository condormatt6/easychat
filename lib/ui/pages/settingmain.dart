import 'package:cached_network_image/cached_network_image.dart';
import 'package:easychat/bloc/authentication/bloc.dart';
import 'package:easychat/repository/userRepository.dart';
import 'package:easychat/ui/pages/editProfile.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingMain extends StatefulWidget {
  final String name;
  final String imageUser;
  final String userId;

  const SettingMain({Key key, this.name, this.imageUser, this.userId})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _SettingMain();
}

class _SettingMain extends State<SettingMain>
    with AutomaticKeepAliveClientMixin<SettingMain> {
  UserRepository _userRepository;

  @override
  bool get wantKeepAlive => true;

  double _animatedHeight = 0;
  bool _manValue = false;
  bool _womanValue = true;

  double _lowerValue = 18.0;
  double _upperValue = 30.0;
  String imageUser;
  File photo;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 20, bottom: 20),
            child: ListTile(
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(100.0),
                child: CachedNetworkImage(
                  imageUrl: widget.imageUser,
                  placeholder: (context, url) => Container(
                    transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                    child: Container(
                        width: 60,
                        height: 80,
                        child: Center(child: new CircularProgressIndicator())),
                  ),
                  errorWidget: (context, url, error) => new Icon(Icons.error),
                  width: 60,
                  height: 80,
                  fit: BoxFit.cover,
                ),
              ),
              title: Text(
                widget.name,
                style: TextStyle(
                    fontFamily: 'Poppins',
                    color: Colors.black38,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Divider(),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 35.0),
            child: Text(
              'Profile',
              style: TextStyle(
                fontFamily: 'Poppins',
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Colors.deepPurple,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 10, 0, 0),
            child: ListTile(
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(40.0),
                child: Icon(
                  Icons.edit,
                  color: Colors.deepPurple,
                ),
              ),
              title: Text(
                'edit profile',
                style: TextStyle(
                    fontSize: 15, fontFamily: 'Poppins', color: Colors.black38),
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => EditProfile()));
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 5, 0, 0),
            child: ListTile(
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(40.0),
                child: Icon(
                  Icons.vpn_key,
                  color: Colors.deepPurple,
                ),
              ),
              title: Text(
                'change password',
                style: TextStyle(
                    fontSize: 15, fontFamily: 'Poppins', color: Colors.black38),
              ),
              onTap: () {},
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 5, 0, 0),
            child: ListTile(
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(40.0),
                child: Icon(
                  Icons.vpn_key,
                  color: Colors.deepPurple,
                ),
              ),
              title: Text(
                'shoogar',
                style: TextStyle(
                    fontSize: 15, fontFamily: 'Poppins', color: Colors.black38),
              ),
              onTap: () {},
            ),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 10, 0, 0),
            child: ListTile(
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(15.0),
                child: Icon(Icons.lock_open, color: Colors.red[900]),
              ),
              title: Text(
                'Log Out',
                style: TextStyle(
                    fontSize: 15,
                    color: Colors.red[900],
                    fontFamily: 'Poppins'),
              ),
              onTap: () {
                _showDialog();
              },
            ),
          ),
        ],
      ),
    );
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Do you want to log out?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new MaterialButton(
              child: new Text(
                "No",
                style: TextStyle(color: Colors.grey),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new MaterialButton(
              child: new Text(
                "Yes",
                style: TextStyle(color: Colors.blue),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
              },
            ),
          ],
        );
      },
    );
  }
}
