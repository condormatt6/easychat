import 'dart:io';

import 'package:easychat/bloc/messaging/bloc.dart';
import 'package:easychat/models/message.dart';
import 'package:easychat/models/user.dart';
import 'package:easychat/repository/messaging.dart';
import 'package:easychat/ui/widgets/message.dart';
import 'package:easychat/ui/widgets/photo.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Messaging extends StatefulWidget {
  final User currentUser, selectedUser;

  const Messaging({this.currentUser, this.selectedUser});

  @override
  _MessagingState createState() => _MessagingState();
}

class _MessagingState extends State<Messaging> {
  TextEditingController _messageTextController = TextEditingController();
  MessagingRepository _messagingRepository = MessagingRepository();
  MessagingBloc _messagingBloc;
  bool isValid = false;
  FocusNode focusNode = FocusNode();
  bool show = false;

//  bool get isPopulated => _messageTextController.text.isNotEmpty;
//
//  bool isSubmitButtonEnabled(MessagingState state) {
//    return isPopulated;
//  }

  @override
  void initState() {
    super.initState();
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        setState(() {
          show = false;
        });
      }
    });

    _messagingBloc = MessagingBloc(messagingRepository: _messagingRepository);

    _messageTextController.text = '';
    _messageTextController.addListener(() {
      setState(() {
        isValid = (_messageTextController.text.isEmpty) ? false : true;
      });
    });
  }

  @override
  void dispose() {
    _messageTextController.dispose();
    super.dispose();
  }

  void _onFormSubmitted() {
    print("Message Submitted");

    _messagingBloc.add(
      SendMessageEvent(
        message: Message(
          text: _messageTextController.text,
          senderId: widget.currentUser.uid,
          senderName: widget.currentUser.name,
          selectedUserId: widget.selectedUser.uid,
          photo: null,
        ),
      ),
    );
    _messageTextController.clear();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        elevation: size.height * 0.02,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            ClipOval(
              child: Container(
                height: size.height * 0.06,
                width: size.height * 0.06,
                child: PhotoWidget(
                  photoLink: widget.selectedUser.photo,
                ),
              ),
            ),
            SizedBox(
              width: size.width * 0.03,
            ),
            Expanded(
              child: Text(widget.selectedUser.name),
            ),
          ],
        ),
      ),
      body: BlocBuilder<MessagingBloc, MessagingState>(
        bloc: _messagingBloc,
        builder: (BuildContext context, MessagingState state) {
          if (state is MessagingInitialState) {
            _messagingBloc.add(
              MessageStreamEvent(
                  currentUserId: widget.currentUser.uid,
                  selectedUserId: widget.selectedUser.uid),
            );
          }
          if (state is MessagingLoadingState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is MessagingLoadedState) {
            Stream<QuerySnapshot> messageStream = state.messageStream;
            return WillPopScope(
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: const AssetImage('assets/images/messagingBack.jpg'),
                    fit: BoxFit.cover,
                    colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.43), BlendMode.dstIn),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    StreamBuilder<QuerySnapshot>(
                      stream: messageStream,
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return Text(
                            "Start the conversation?",
                            style: TextStyle(
                                fontSize: 16.0, fontWeight: FontWeight.bold),
                          );
                        }
                        if (snapshot.data.docs.isNotEmpty) {
                          return Expanded(
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: ListView.builder(
                                    reverse: true,
                                    shrinkWrap: true,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return MessageWidget(
                                        currentUserId: widget.currentUser.uid,
                                        messageId: snapshot.data.docs[index].id,
                                      );
                                    },
                                    itemCount: snapshot.data.docs.length,
                                  ),
                                )
                              ],
                            ),
                          );
                        } else {
                          return Center(
                            child: Text(
                              "Start the conversation ?",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          );
                        }
                      },
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width - 60,
                                child: Card(
                                  // color: kPrimaryColor,
                                  margin: const EdgeInsets.only(
                                      left: 13.0, right: 2.0, bottom: 8.0),
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(25),
                                    ),
                                  ),
                                  child: TextFormField(
                                    textInputAction: TextInputAction.send,
                                    controller: _messageTextController,
                                    focusNode: focusNode,
                                    textAlignVertical: TextAlignVertical.center,
                                    keyboardType: TextInputType.multiline,
                                    maxLines: 5,
                                    minLines: 1,
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      prefixIcon: IconButton(
                                        onPressed: () {
                                          focusNode.unfocus();
                                          focusNode.canRequestFocus = false;
                                          setState(() {
                                            show = !show;
                                          });
                                        },
                                        icon: const Icon(Icons.emoji_emotions),
                                      ),
                                      hintText: 'Type a message',
                                      contentPadding: const EdgeInsets.all(5),
                                      suffixIcon: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          IconButton(
                                            onPressed: () {
                                              focusNode.unfocus();
                                              focusNode.canRequestFocus = false;
                                            },
                                            icon: const Icon(Icons.attach_file),
                                          ),
                                          GestureDetector(
                                            onTap: () async {
                                              focusNode.unfocus();
                                              focusNode.canRequestFocus = false;

                                              File photo = await File(
                                                  (await FilePicker.platform
                                                          .pickFiles())
                                                      .files
                                                      .single
                                                      .path);

                                              if (photo != null) {
                                                _messagingBloc.add(
                                                  SendMessageEvent(
                                                    message: Message(
                                                        text: null,
                                                        senderName: widget
                                                            .currentUser.name,
                                                        senderId: widget
                                                            .currentUser.uid,
                                                        photo: photo,
                                                        selectedUserId: widget
                                                            .selectedUser.uid),
                                                  ),
                                                );
                                              }
                                            },
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(
                                                horizontal: size.height * 0.005,
                                              ),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 8.0),
                                                child: Icon(
                                                  Icons.camera_alt,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 8.0, right: 5, left: 2),
                                child: GestureDetector(
                                  onTap: () {
                                    isValid ? _onFormSubmitted() : null;
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: size.height * 0.01),
                                    child: Icon(
                                      Icons.send,
                                      size: size.height * 0.04,
                                      color: isValid
                                          ? Colors.deepPurple
                                          : Colors.black,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          show
                              ? Container(
                                  height: 250,
                                  width: double.infinity,
                                  child: emojiselect(),
                                )
                              : Container(),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              onWillPop: () {
                if (show) {
                  setState(() {
                    show = false;
                  });
                } else {
                  Navigator.pop(context);
                }
                return Future.value(false);
              },
            );
          }
          return Container();
        },
      ),
    );
  }

  Widget emojiselect() {
    return EmojiPicker(
        config: const Config(
          columns: 7,
        ),
        onEmojiSelected: (category, emoji) {
          // print(emoji);
          setState(() {
            _messageTextController.text += emoji.emoji;
          });
        });
  }
}
