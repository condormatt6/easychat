import 'package:easychat/bloc/profession/bloc.dart';
import 'package:easychat/repository/userRepository.dart';
import 'package:easychat/ui/widgets/professionForm.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Profession extends StatelessWidget {
  final _userRepository;

  Profession({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<ProfessionBloc>(
        create: (context) => ProfessionBloc(userRepository: _userRepository),
        child: ProfessionForm(
          userRepository: _userRepository,
        ),
      ),
    );
  }
}
