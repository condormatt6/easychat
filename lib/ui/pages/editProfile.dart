import 'package:easychat/repository/userRepository.dart';
import 'package:easychat/ui/widgets/nameForm.dart';
import 'package:extended_image/extended_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final TextEditingController _nameController = TextEditingController();
  UserRepository _userRepository;
  String userId;
  File photo;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Profile'),
        backgroundColor: Colors.deepPurple,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: size.width * 0.4),
                Text(
                  'Change your information',
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 25,
                    color: Colors.deepPurple,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: size.height * 0.02,
                ),
                Text(
                  'Your name & profile',
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 10,
                    color: Colors.deepPurple,
                  ),
                ),
                SizedBox(height: size.height * 0.02),
                Container(
                  width: size.width,
                  child: CircleAvatar(
                    radius: size.width * 0.15,
                    backgroundColor: Colors.transparent,
                    child: photo == null
                        ? GestureDetector(
                            onTap: () async {
                              File getPic = File(
                                  (await FilePicker.platform.pickFiles())
                                      .files
                                      .single
                                      .path);
                              if (getPic != null) {
                                setState(() {
                                  photo = getPic;
                                });
                              }
                            },
                            child: Image.asset('assets/profilephoto.png'),
                          )
                        : GestureDetector(
                            onTap: () async {
                              File getPic = File(
                                  (await FilePicker.platform.pickFiles())
                                      .files
                                      .single
                                      .path);
                              if (getPic != null) {
                                setState(() {
                                  photo = getPic;
                                });
                              }
                            },
                            child: CircleAvatar(
                              radius: size.width * 0.2,
                              backgroundImage: FileImage(photo),
                            ),
                          ),
                  ),
                ),
                SizedBox(height: size.height * 0.07),
                textFieldWidget(_nameController, "Nom", size),
                SizedBox(height: size.height * 0.03),
                InkWell(
                  onTap: () {},
                  child: Container(
                    width: size.width * 0.4,
                    height: size.height * 0.06,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Color.fromRGBO(143, 148, 251, .2),
                            blurRadius: 10,
                            offset: Offset(0, 8))
                      ],
                      color: Colors.deepPurple,
                      borderRadius: BorderRadius.circular(size.height * 0.05),
                    ),
                    child: Center(
                        child: Text(
                      "Save",
                      style: TextStyle(
                          fontSize: size.height * 0.017,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    )),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
