import 'package:easychat/bloc/authentication/bloc.dart';
import 'package:easychat/repository/userRepository.dart';
import 'package:easychat/ui/pages/name.dart';
import 'package:easychat/ui/pages/profession.dart';
import 'package:easychat/ui/pages/profile.dart';
import 'package:easychat/ui/pages/splash.dart';
import 'package:easychat/ui/widgets/tabs.dart';
import 'package:easychat/ui/widgets/type_shoogar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'login.dart';

class Home extends StatelessWidget {
  final UserRepository _userRepository;

  Home({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is Uninitialized) {
            return Splash();
          }
          if (state is Authenticated) {
            return Tabs(
              userId: state.userId,
            );
          }
          if (state is AuthenticatedButNotSet) {
            return Name(
              userRepository: _userRepository,
              userId: state.userId,
            );
          }
          if (state is AuthenticatedButSetProfession) {
            return Profile(userRepository: _userRepository);
          }
          if (state is AuthenticatedButSetNameAndPhoto) {
            return Profession(userRepository: _userRepository);
          }

          if (state is Unauthenticated) {
            return Login(
              userRepository: _userRepository,
            );
          } else
            return Container();
        },
      ),
    );
  }
}
