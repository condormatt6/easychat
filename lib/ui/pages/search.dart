import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easychat/bloc/search/bloc.dart';
import 'package:easychat/models/user.dart';
import 'package:easychat/repository/searchRepository.dart';
import 'package:easychat/repository/userRepository.dart';
import 'package:easychat/ui/widgets/iconWidget.dart';
import 'package:easychat/ui/widgets/profile.dart';
import 'package:easychat/ui/widgets/userGender.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';

import 'package:swipe_to/swipe_to.dart';

class Search extends StatefulWidget {
  final String userId;

  const Search({this.userId});

  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  final SearchRepository _searchRepository = SearchRepository();
  SearchBloc _searchBloc;

  UserRepository _userRepository;
  User _user, _currentUser;
  int difference;
  GeoPoint userLocat;

  getDifference(GeoPoint userLocation) async {
    Position position = await Geolocator.getCurrentPosition();
    double location = await Geolocator.distanceBetween(
        (userLocation.longitude).toDouble(),
        (userLocation.latitude).toDouble(),
        (position.longitude).toDouble(),
        (position.latitude).toDouble());

    difference = location.toInt();
  }

  @override
  void initState() {
    _searchBloc = SearchBloc(searchRepository: _searchRepository);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return BlocBuilder<SearchBloc, SearchState>(
      bloc: _searchBloc,
      builder: (context, state) {
        if (state is InitialSearchState) {
          _searchBloc.add(
            LoadUserEvent(userId: widget.userId),
          );
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.blueGrey),
            ),
          );
        }
        if (state is LoadingState) {
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.deepPurple),
            ),
          );
        }
        if (state is LoadUserState) {
          _user = state.user;
          _currentUser = state.currentUser;
          print('lutilisateur ${_currentUser.name}');

          getDifference(_user.location);
          print('lutilisateur ${_user.name}');

          if (_user.location == null) {
            return Center(
              child: InkWell(
                onTap: () {
                  print('pop');
                },
                child: Image(
                  image: AssetImage('assets/images/search.png'),
                  height: 200,
                  width: 200,
                ),
              ),
            );
          } else
            return SwipeTo(
              onRightSwipe: () {
                _searchBloc.add(PassUserEvent(widget.userId, _user.uid));
              },
              onLeftSwipe: () {
                _searchBloc.add(
                  SelectUserEvent(
                      name: _currentUser.name,
                      photoUrl: _currentUser.photo,
                      currentUserId: widget.userId,
                      selectedUserId: _user.uid),
                );
              },
              child: profileWidget(
                padding: size.height * 0.015,
                photoHeight: size.height * 0.95,
                photoWidth: size.width * 0.95,
                photo: _user.photo,
                clipRadius: size.height * 0.05,
                containerHeight: size.height * 0.3,
                containerWidth: size.width * 0.95,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: size.width * 0.02),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: [
                      BoxShadow(color: Colors.black12, spreadRadius: 0.5),
                    ],
                    gradient: LinearGradient(
                      colors: [Colors.black12, Colors.black87],
                      begin: Alignment.center,
                      stops: [0.4, 1],
                      end: Alignment.bottomCenter,
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: size.height * 0.04,
                      ),
                      Row(
                        children: <Widget>[
                          userGender(_user.gender),
                          Expanded(
                            child: Text(
                              " " +
                                  _user.name +
                                  ", " +
                                  (DateTime.now().year -
                                          _user.age.toDate().year)
                                      .toString(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: size.height * 0.05,
                                  fontFamily: 'arial'),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.location_on,
                            color: Colors.white,
                          ),
                          Text(
                            difference != null
                                ? (difference / 1000).floor().toString() +
                                    " km away"
                                : "away",
                            style: TextStyle(
                                color: Colors.white, fontFamily: 'poppins'),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: size.height * 0.02,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.work_sharp,
                            color: Colors.white,
                          ),
                          Text(
                            " " + _user.profession + " ",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: size.height * 0.03,
                                fontFamily: 'arial'),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          // iconWidget(EvaIcons.flash, () {}, size.height * 0.04,
                          //     Colors.yellow),
                          iconWidget(Icons.clear, () {
                            _searchBloc
                                .add(PassUserEvent(widget.userId, _user.uid));
                          }, size.height * 0.08, Colors.blue),
                          iconWidget(FontAwesomeIcons.solidHeart, () {
                            _searchBloc.add(
                              SelectUserEvent(
                                  name: _currentUser.name,
                                  photoUrl: _currentUser.photo,
                                  currentUserId: widget.userId,
                                  selectedUserId: _user.uid),
                            );
                          }, size.height * 0.06, Colors.red),
                          // iconWidget(EvaIcons.options2, () {}, size.height * 0.04,
                          //     Colors.white)
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
        } else
          return Container();
      },
    );
  }
}
