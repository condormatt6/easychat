import 'package:easychat/bloc/profile/bloc.dart';
import 'package:easychat/repository/userRepository.dart';
import 'package:easychat/ui/widgets/profileForm.dart';
import 'package:extended_image/extended_image.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Profile extends StatelessWidget {
  final _userRepository;

  Profile({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("Profile Setup"),
      //   centerTitle: true,
      //   backgroundColor: backgroundColor,
      //   elevation: 0,
      // ),
      body: BlocProvider<ProfileBloc>(
        create: (context) => ProfileBloc(userRepository: _userRepository),
        child: ProfileForm(
          userRepository: _userRepository,
        ),
      ),
    );
  }
}
