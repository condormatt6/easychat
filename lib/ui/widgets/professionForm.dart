import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easychat/bloc/authentication/bloc.dart';
import 'package:easychat/bloc/profession/bloc.dart';
import 'package:easychat/repository/userRepository.dart';
import 'package:easychat/ui/pages/profile.dart';
import 'package:easychat/ui/widgets/type_shoogar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProfessionForm extends StatefulWidget {
  final _userRepository;

  ProfessionForm({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  _ProfessionFormState createState() => _ProfessionFormState();
}

class _ProfessionFormState extends State<ProfessionForm> {
  UserRepository get _userRepository => widget._userRepository;
  final TextEditingController _professionController = TextEditingController();

  String userId;
  ProfessionBloc _professionBloc;

  bool get isFilled => _professionController.text.isNotEmpty;

  bool isSubmittingName(ProfessionState state) {
    return isFilled && !state.isSubmitting;
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    _professionBloc = BlocProvider.of<ProfessionBloc>(context);
  }

  _onFormSubmitted() {
    _professionBloc.add(Submitted(profession: _professionController.text));
  }

  @override
  void dispose() {
    _professionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return BlocListener<ProfessionBloc, ProfessionState>(
      listener: (context, state) {
        if (state.isFailure) {
          print("Failed");
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Profile Creation Unsuccesful'),
                    Icon(Icons.error)
                  ],
                ),
              ),
            );
        }
        if (state.isSubmitting) {
          print("Submitting");
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Submitting'),
                    CircularProgressIndicator()
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          print("Success!");
          BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());

          // Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          //   return Shoogar();
          // }));
          // Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          //   return Profile(userRepository: _userRepository);
          // }));
        }
      },
      child: BlocBuilder<ProfessionBloc, ProfessionState>(
        builder: (context, state) {
          return Scaffold(
            body: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: size.width * 0.7),
                      Text(
                        'What are you doing?',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 30,
                          color: Colors.deepPurple,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.02,
                      ),
                      Text(
                        'Profession',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 10,
                          color: Colors.deepPurple,
                        ),
                      ),
                      SizedBox(height: size.height * 0.07),
                      textFieldWidget(
                          _professionController, "ProfessionForm ", size),
                      SizedBox(height: size.height * 0.03),
                      Container(
                        width: size.width * 0.4,
                        height: size.height * 0.06,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(143, 148, 251, .2),
                                blurRadius: 10,
                                offset: Offset(0, 8))
                          ],
                          color: Colors.deepPurple,
                          borderRadius:
                              BorderRadius.circular(size.height * 0.05),
                        ),
                        child: Center(
                            child: InkWell(
                          onTap: () {
                            if (isSubmittingName(state)) {
                              print('clické');
                              _onFormSubmitted();
                              print(state.isSuccess);
                              print(state.isSubmitting);
                            } else {
                              print('non clické');
                            }
                          },
                          child: Text(
                            "Continue",
                            style: TextStyle(
                                fontSize: size.height * 0.017,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        )),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

Widget textFieldWidget(controller, text, size) {
  return Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Color.fromRGBO(143, 148, 251, .2),
              blurRadius: 10,
              offset: Offset(0, 8))
        ]),
    padding: EdgeInsets.all(size.height * 0.01),
    child: TextField(
      controller: controller,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.card_travel),
        isDense: true,
        hintText: text,
        labelStyle:
            TextStyle(color: Colors.deepPurple, fontSize: size.height * 0.03),
        // focusedBorder: OutlineInputBorder(
        //   borderSide: BorderSide(color: Colors.white, width: 1.0),
        // ),
        // enabledBorder: OutlineInputBorder(
        //   borderSide: BorderSide(color: Colors.white, width: 1.0),
        // ),
        border: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(25.0),
          ),
          borderSide: BorderSide(
            width: 0,
            style: BorderStyle.none,
          ),
        ),
      ),
    ),
  );
}
