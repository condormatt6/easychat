import 'package:easychat/bloc/authentication/bloc.dart';
import 'package:easychat/bloc/name/bloc.dart';
import 'package:easychat/repository/userRepository.dart';
import 'package:extended_image/extended_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NameForm extends StatefulWidget {
  final _userRepository;

  NameForm({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;
  @override
  _NameFormState createState() => _NameFormState();
}

class _NameFormState extends State<NameForm> {
  final TextEditingController _nameController = TextEditingController();
  UserRepository get _userRepository => widget._userRepository;
  String userId;
  NameBloc _nameBloc;
  File photo;

  bool get isFilled => _nameController.text.isNotEmpty && photo != null;

  bool isSubmittingName(NameState state) {
    return isFilled && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();

    _nameBloc = BlocProvider.of<NameBloc>(context);
  }

  _onFormSubmitted() {
    _nameBloc.add(Submitted(name: _nameController.text, photo: photo));
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return BlocListener<NameBloc, NameState>(
      listener: (context, state) {
        if (state.isFailure) {
          print("Failed");
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Profile Creation Unsuccesful'),
                    Icon(Icons.error)
                  ],
                ),
              ),
            );
        }
        if (state.isSubmitting) {
          print("Submitting");
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Submitting'),
                    CircularProgressIndicator()
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          print("Success!");
          BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());

          // Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          //   return Profession(
          //     userRepository: _userRepository,
          //   );
          // }));
        }
      },
      child: BlocBuilder<NameBloc, NameState>(
        builder: (context, state) {
          return Scaffold(
            body: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: size.width * 0.4),
                      Text(
                        'Who are you?',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 35,
                          color: Colors.deepPurple,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.02,
                      ),
                      Text(
                        'Your name & profile',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 10,
                          color: Colors.deepPurple,
                        ),
                      ),
                      SizedBox(height: size.height * 0.02),
                      Container(
                        width: size.width,
                        child: CircleAvatar(
                          radius: size.width * 0.15,
                          backgroundColor: Colors.transparent,
                          child: photo == null
                              ? GestureDetector(
                                  onTap: () async {
                                    File getPic = File(
                                        (await FilePicker.platform.pickFiles())
                                            .files
                                            .single
                                            .path);
                                    if (getPic != null) {
                                      setState(() {
                                        photo = getPic;
                                      });
                                    }
                                  },
                                  child: Image.asset('assets/profilephoto.png'),
                                )
                              : GestureDetector(
                                  onTap: () async {
                                    File getPic = File(
                                        (await FilePicker.platform.pickFiles())
                                            .files
                                            .single
                                            .path);
                                    if (getPic != null) {
                                      setState(() {
                                        photo = getPic;
                                      });
                                    }
                                  },
                                  child: CircleAvatar(
                                    radius: size.width * 0.2,
                                    backgroundImage: FileImage(photo),
                                  ),
                                ),
                        ),
                      ),
                      SizedBox(height: size.height * 0.07),
                      textFieldWidget(_nameController, "Nom", size),
                      SizedBox(height: size.height * 0.03),
                      InkWell(
                        onTap: () {
                          if (isSubmittingName(state)) {
                            print('clické');
                            _onFormSubmitted();
                            print(state.isSuccess);
                            print(state.isSubmitting);
                          } else {
                            print('non clické');
                          }
                        },
                        child: Container(
                          width: size.width * 0.4,
                          height: size.height * 0.06,
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(143, 148, 251, .2),
                                  blurRadius: 10,
                                  offset: Offset(0, 8))
                            ],
                            color: Colors.deepPurple,
                            borderRadius:
                                BorderRadius.circular(size.height * 0.05),
                          ),
                          child: Center(
                              child: Text(
                            "Continue",
                            style: TextStyle(
                                fontSize: size.height * 0.017,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          )),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

Widget textFieldWidget(controller, text, size) {
  return Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Color.fromRGBO(143, 148, 251, .2),
              blurRadius: 10,
              offset: Offset(0, 8))
        ]),
    padding: EdgeInsets.all(size.height * 0.01),
    child: TextField(
      controller: controller,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.account_circle_outlined),
        isDense: true,
        hintText: text,
        labelStyle:
            TextStyle(color: Colors.deepPurple, fontSize: size.height * 0.03),
        // focusedBorder: OutlineInputBorder(
        //   borderSide: BorderSide(color: Colors.white, width: 1.0),
        // ),
        // enabledBorder: OutlineInputBorder(
        //   borderSide: BorderSide(color: Colors.white, width: 1.0),
        // ),
        border: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(25.0),
          ),
          borderSide: BorderSide(
            width: 0,
            style: BorderStyle.none,
          ),
        ),
      ),
    ),
  );
}
