import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easychat/ui/pages/matches.dart';
import 'package:easychat/ui/pages/messages.dart';
import 'package:easychat/ui/pages/search.dart';
import 'package:easychat/ui/pages/settingmain.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Tabs extends StatefulWidget {
  final userId;
  const Tabs({this.userId});

  @override
  State<Tabs> createState() => _TabsState();
}

class _TabsState extends State<Tabs> {
  String name;
  String imageUser;
  String id;
  @override
  void initState() {
    super.initState();
    final firestoreInstance = FirebaseFirestore.instance;

    var firebaseUser;
    firebaseUser = FirebaseAuth.instance.currentUser;
    firestoreInstance
        .collection("users")
        .doc(firebaseUser.uid)
        .get()
        .then((value) {
      print(value.data()['name']);
      setState(() {
        name = value.data()['name'];
      });
    });
    firebaseUser = FirebaseAuth.instance.currentUser;
    firestoreInstance
        .collection("users")
        .doc(firebaseUser.uid)
        .get()
        .then((value) {
      print(value.data()['photoUrl']);
      setState(() {
        imageUser = value.data()['photoUrl'];
      });
    });
    id = FirebaseAuth.instance.currentUser.uid;
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> pages = [
      Search(
        userId: id,
      ),
      Matches(
        userId: widget.userId,
      ),
      Messages(
        userId: widget.userId,
      ),
      SettingMain(
        name: name,
        imageUser: imageUser,
        userId: widget.userId,
      ),
    ];

    return Theme(
      data: ThemeData(
        primaryColor: Colors.deepPurple,
      ),
      child: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              "Shoogar",
              style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Poppins'),
            ),
            // actions: <Widget>[
            //   IconButton(
            //     icon: Icon(Icons.exit_to_app),
            //     onPressed: () {
            //       BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
            //     },
            //   )
            // ],
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(60.0),
              child: Container(
                height: 75.0,
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TabBar(
                      tabs: <Widget>[
                        Tab(
                          text: 'Discover',
                          icon: Icon(Icons.search),
                        ),
                        Tab(
                          icon: Icon(Icons.favorite),
                          text: 'Matches',
                        ),
                        Tab(
                          icon: Icon(Icons.chat_bubble_outline),
                          text: 'Chat',
                        ),
                        Tab(
                          icon: Icon(Icons.account_circle_outlined),
                          text: 'Profil',
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: pages,
          ),
        ),
      ),
    );
  }
}
