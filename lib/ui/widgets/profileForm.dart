import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easychat/bloc/authentication/bloc.dart';
import 'package:easychat/bloc/profile/bloc.dart';
import 'package:easychat/repository/userRepository.dart';
import 'package:easychat/ui/pages/home.dart';
import 'package:easychat/ui/widgets/gender.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:date_time_picker/date_time_picker.dart';
import '../constants.dart';

class ProfileForm extends StatefulWidget {
  final UserRepository _userRepository;

  ProfileForm({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  _ProfileFormState createState() => _ProfileFormState();
}

class _ProfileFormState extends State<ProfileForm> {
  UserRepository get _userRepository => widget._userRepository;

  String gender, interestedIn;
  DateTime age;
  GeoPoint location;
  ProfileBloc _profileBloc;

  //UserRepository get _userRepository => widget._userRepository;

  bool get isFilled => gender != null && interestedIn != null && age != null;

  bool isButtonEnabled(ProfileState state) {
    return isFilled && !state.isSubmitting;
  }

  _getLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    location = GeoPoint(position.latitude, position.longitude);
  }

  _onSubmitted() async {
    await _getLocation();
    _profileBloc.add(
      Submitted(
          age: age,
          location: location,
          interestedIn: interestedIn,
          gender: gender),
    );
  }

  @override
  void initState() {
    _getLocation();
    // print(widget.name + 'et' + widget.profession);
    _profileBloc = BlocProvider.of<ProfileBloc>(context);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return BlocListener<ProfileBloc, ProfileState>(
      //bloc: _profileBloc,
      listener: (context, state) {
        if (state.isFailure) {
          print("Failed");
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Profile Creation Unsuccesful'),
                    Icon(Icons.error)
                  ],
                ),
              ),
            );
        }
        if (state.isSubmitting) {
          print("Submitting");
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Submitting'),
                    CircularProgressIndicator()
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          print("Success!");
          BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
          // Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          //   return Home(userRepository: _userRepository);
          // }));
        }
      },
      child: BlocBuilder<ProfileBloc, ProfileState>(
        builder: (context, state) {
          return SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              padding: const EdgeInsets.all(35.0),
              color: Colors.white,
              width: size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: size.height * 0.02),
                  SizedBox(height: size.height * 0.1),
                  Text(
                    'Shoogar Profile',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 35,
                      fontWeight: FontWeight.bold,
                      color: Colors.deepPurple,
                    ),
                  ),
                  SizedBox(height: size.height * 0.03),
                  SizedBox(height: size.height * 0.03),
                  SizedBox(height: 15),
                  Text(
                    'Birthday',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: size.height * 0.02,
                      color: Colors.deepPurple,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 15.0),

                  //GestureDetector(
                  //   onTap: () {
                  //     DatePicker.showDatePicker(
                  //       context,
                  //       showTitleActions: true,
                  //       minTime: DateTime(1900, 1, 1),
                  //       maxTime: DateTime(DateTime.now().year - 11, 1, 1),
                  //       theme: DatePickerTheme(
                  //           headerColor: Colors.deepPurple,
                  //           backgroundColor: Colors.white,
                  //           itemStyle: TextStyle(
                  //               color: Colors.black,
                  //               fontWeight: FontWeight.bold,
                  //               fontSize: 18),
                  //           doneStyle:
                  //               TextStyle(color: Colors.white, fontSize: 16)),
                  //       onConfirm: (date) {
                  //         setState(() {
                  //           age = date;
                  //         });
                  //         print(age);
                  //       },
                  //     );
                  //   },
                  //   child: Container(
                  //     width: size.width * 0.7,
                  //     height: size.height * 0.06,
                  //     decoration: BoxDecoration(
                  //       color: Colors.deepPurple,
                  //       borderRadius: BorderRadius.circular(size.height * 0.05),
                  //     ),
                  //     child: Center(
                  //       child: Text(
                  //         "Enter Birthday",
                  //         style: TextStyle(
                  //           color: Colors.white,
                  //           fontSize: size.height * 0.025,
                  //           fontFamily: 'Poppins',
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  DateTimePicker(
                    initialValue: '',
                    firstDate: DateTime(1980),
                    lastDate: DateTime(2022),
                    dateLabelText: 'Date',
                    onChanged: (val) {
                      setState(() {
                        age = DateTime.parse(val);
                      });
                      print("age : $age");
                    },
                    validator: (val) {
                      print(val);

                      return null;
                    },
                    onSaved: (val) => print(val),
                  ),
                  SizedBox(
                    height: size.height * 0.04,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: size.height * 0.02),
                        child: Text(
                          "Sexe",
                          style: TextStyle(
                              color: Colors.deepPurple,
                              fontSize: size.width * 0.03,
                              fontFamily: 'Poppins'),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          genderWidget(
                              FontAwesomeIcons.venus, "Female", size, gender,
                              () {
                            setState(() {
                              gender = "Female";
                            });
                          }),
                          genderWidget(
                              FontAwesomeIcons.mars, "Male", size, gender, () {
                            setState(() {
                              gender = "Male";
                            });
                          }),
                          // genderWidget(
                          //   FontAwesomeIcons.transgender,
                          //   "Transgender",
                          //   size,
                          //   gender,
                          //   () {
                          //     setState(
                          //       () {
                          //         gender = "Transgender";
                          //       },
                          //     );
                          //   },
                          // ),
                        ],
                      ),
                      SizedBox(
                        height: size.height * 0.03,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: size.height * 0.02),
                        child: Text(
                          "Looking For",
                          style: TextStyle(
                              color: Colors.deepPurple,
                              fontSize: size.width * 0.03,
                              fontFamily: 'Poppins'),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          genderWidget(FontAwesomeIcons.venus, "Female", size,
                              interestedIn, () {
                            setState(() {
                              interestedIn = "Female";
                            });
                          }),
                          genderWidget(
                              FontAwesomeIcons.mars, "Male", size, interestedIn,
                              () {
                            setState(() {
                              interestedIn = "Male";
                            });
                          }),
                          // genderWidget(
                          //   FontAwesomeIcons.transgender,
                          //   "Transgender",
                          //   size,
                          //   interestedIn,
                          //   () {
                          //     setState(
                          //       () {
                          //         interestedIn = "Transgender";
                          //       },
                          //     );
                          //   },
                          // ),
                        ],
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: size.height * 0.02),
                    child: GestureDetector(
                      onTap: () {
                        if (isButtonEnabled(state)) {
                          _onSubmitted();
                        } else {}
                      },
                      child: Container(
                        width: size.width * 0.6,
                        height: size.height * 0.06,
                        decoration: BoxDecoration(
                          color: isButtonEnabled(state)
                              ? Colors.deepPurple
                              : Colors.black26,
                          borderRadius:
                              BorderRadius.circular(size.height * 0.05),
                        ),
                        child: Center(
                            child: Text(
                          "Save",
                          style: TextStyle(
                              fontSize: size.height * 0.025,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        )),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
