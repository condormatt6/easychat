import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easychat/ui/pages/payment.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';

enum SingingCharacter { ShMommy, ShDaddy, ShBabies }

class Shoogar extends StatefulWidget {
  bool _ShMommy = false;
  bool _ShDaddy = true;
  bool _ShBaby = false;
  @override
  _ShoogarState createState() => _ShoogarState();
}

class _ShoogarState extends State<Shoogar> {
  bool _ShMommy = false;
  bool _ShDaddy = true;
  bool _ShBaby = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(35.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 200),
            Text(
              'Shoogar Daddy or Baby',
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 35,
                color: Colors.deepPurple,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 5),
            Text(
              'Wich one you are ?.',
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 10,
                color: Colors.deepPurple,
              ),
            ),
            SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: Row(
                children: [
                  Checkbox(
                    value: _ShMommy,
                    onChanged: (bool newValue) {
                      setState(() {
                        _ShMommy = newValue;
                      });
                    },
                    activeColor: Colors.deepPurple,
                  ),
                  GestureDetector(
                    child: Text("ShoogarMommy",
                        style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'Poppins',
                            color: Colors.deepPurple)),
                    onTap: () {
                      setState(() {
                        _ShMommy = !_ShMommy;
                      });
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: 5),
            Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: Row(
                children: [
                  Checkbox(
                    value: _ShDaddy,
                    onChanged: (bool newValue) {
                      setState(() {
                        _ShDaddy = newValue;
                      });
                    },
                    activeColor: Colors.deepPurple,
                  ),
                  GestureDetector(
                    child: Text("ShoogarDaddy",
                        style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'Poppins',
                            color: Colors.deepPurple)),
                    onTap: () {
                      setState(() {
                        _ShDaddy = !_ShDaddy;
                      });
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: 5),
            Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: Row(
                children: [
                  Checkbox(
                    value: _ShBaby,
                    onChanged: (bool newValue) {
                      setState(() {
                        _ShBaby = newValue;
                      });
                    },
                    activeColor: Colors.deepPurple,
                  ),
                  GestureDetector(
                    child: Text("ShoogarBabie",
                        style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'Poppins',
                            color: Colors.deepPurple)),
                    onTap: () {
                      setState(() {
                        _ShBaby = !_ShBaby;
                      });
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: 50),
            Container(
              width: size.width * 0.4,
              height: size.height * 0.06,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(143, 148, 251, .2),
                      blurRadius: 10,
                      offset: Offset(0, 8))
                ],
                color: Colors.deepPurple,
                borderRadius: BorderRadius.circular(size.height * 0.05),
              ),
              child: Center(
                  child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => PaymentScreen()));
                },
                child: Text(
                  "Save",
                  style: TextStyle(
                      fontSize: size.height * 0.017,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
              )),
            ),
          ],
        ),
      ),
    );
  }
}
