import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class NameEvent extends Equatable {
  const NameEvent();

  @override
  List<Object> get props => [];
}

class NameChanged extends NameEvent {
  final String name;

  NameChanged({@required this.name});

  @override
  List<Object> get props => [name];
}

class PhotoChanged extends NameEvent {
  final File photo;

  PhotoChanged({@required this.photo});

  @override
  List<Object> get props => [photo];
}

class Submitted extends NameEvent {
  final String name;
  final File photo;
  Submitted({
    @required this.name,
    @required this.photo,
  });
  @override
  List<Object> get props => [name, photo];
}
