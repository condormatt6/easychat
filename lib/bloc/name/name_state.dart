import 'package:meta/meta.dart';

@immutable
class NameState {
  final bool isNameEmpty;
  final bool isPhotoEmpty;
  final bool isFailure;
  final bool isSubmitting;
  final bool isSuccess;

  bool get isFormValid => isNameEmpty && isPhotoEmpty;

  NameState({
    @required this.isPhotoEmpty,
    @required this.isNameEmpty,
    @required this.isFailure,
    @required this.isSubmitting,
    @required this.isSuccess,
  });
  factory NameState.empty() {
    return NameState(
      isNameEmpty: false,
      isPhotoEmpty: false,
      isFailure: false,
      isSuccess: false,
      isSubmitting: false,
    );
  }
  factory NameState.loading() {
    return NameState(
      isNameEmpty: false,
      isPhotoEmpty: false,
      isFailure: false,
      isSuccess: false,
      isSubmitting: true,
    );
  }
  factory NameState.failure() {
    return NameState(
      isNameEmpty: false,
      isPhotoEmpty: false,
      isFailure: true,
      isSuccess: false,
      isSubmitting: false,
    );
  }
  factory NameState.success() {
    return NameState(
      isNameEmpty: false,
      isPhotoEmpty: false,
      isFailure: false,
      isSuccess: true,
      isSubmitting: false,
    );
  }
  NameState update({bool isNameEmpty, bool isPhotoEmpty}) {
    return copyWith(
      isNameEmpty: isNameEmpty,
      isPhotoEmpty: isPhotoEmpty,
      isFailure: false,
      isSuccess: false,
      isSubmitting: false,
    );
  }

  NameState copyWith({
    bool isNameEmpty,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    bool isPhotoEmpty,
  }) {
    return NameState(
        isNameEmpty: isNameEmpty ?? this.isNameEmpty,
        isPhotoEmpty: isPhotoEmpty ?? this.isPhotoEmpty,
        isFailure: isFailure ?? this.isFailure,
        isSubmitting: isSubmitting ?? this.isSubmitting,
        isSuccess: isSuccess ?? this.isSuccess);
  }
}
