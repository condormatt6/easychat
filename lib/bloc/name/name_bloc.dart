import 'dart:async';
import 'dart:io';
import 'package:easychat/repository/userRepository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';
import './bloc.dart';
import 'package:bloc/bloc.dart';

class NameBloc extends Bloc<NameEvent, NameState> {
  UserRepository _userRepository;
  FirebaseAuth _firebaseAuth;

  NameBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  NameState get initialState => NameState.empty();

  @override
  Stream<NameState> mapEventToState(NameEvent event) async* {
    if (event is NameChanged) {
      yield* _mapNameChangedToState(event.name);
    } else if (event is PhotoChanged) {
      yield* _mapPhotoChangedToState(event.photo);
    } else if (event is Submitted) {
      final uid = await _userRepository.getUser();

      yield* _mapSubmittedToState(
          name: event.name, photo: event.photo, userId: uid);
    }
  }

  Stream<NameState> _mapNameChangedToState(String name) async* {
    yield state.update(
      isNameEmpty: name == null,
    );
  }

  Stream<NameState> _mapPhotoChangedToState(File photo) async* {
    yield state.update(
      isPhotoEmpty: photo == null,
    );
  }

  Stream<NameState> _mapSubmittedToState(
      {String name, File photo, String userId}) async* {
    try {
      await _userRepository.photoAndImageSetup(photo, name, userId);
      yield NameState.success();
    } catch (_) {
      print('db erreur');
      yield NameState.failure();
    }
  }
}
