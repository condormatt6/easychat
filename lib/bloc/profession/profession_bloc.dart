import 'package:easychat/repository/userRepository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import 'bloc.dart';

class ProfessionBloc extends Bloc<ProfessionEvent, ProfessionState> {
  UserRepository _userRepository;
  ProfessionBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;
  @override
  ProfessionState get initialState => ProfessionState.empty();

  @override
  Stream<ProfessionState> mapEventToState(ProfessionEvent event) async* {
    if (event is ProfessionChanged) {
      yield* _mapProfessionChangedToState(event.profession);
    } else if (event is Submitted) {
      final uid = await _userRepository.getUser();

      yield* _mapSubmittedToState(profession: event.profession, userId: uid);
    }
  }

  Stream<ProfessionState> _mapProfessionChangedToState(
      String profession) async* {
    yield state.update(
      isProfessionEmpty: profession == null,
    );
  }

  Stream<ProfessionState> _mapSubmittedToState(
      {String profession, String userId}) async* {
    try {
      await _userRepository.setupProfession(profession, userId);
      yield ProfessionState.success();
    } catch (_) {
      print('db erreur');
      yield ProfessionState.failure();
    }
  }
}
