import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ProfessionEvent extends Equatable {
  const ProfessionEvent();
  @override
  List<Object> get props => [];
}

class ProfessionChanged extends ProfessionEvent {
  final String profession;
  ProfessionChanged({this.profession});
  @override
  List<Object> get props => [profession];
}

class Submitted extends ProfessionEvent {
  final profession;
  Submitted({@required this.profession});
  @override
  List<Object> get props => [profession];
}
