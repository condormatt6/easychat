import 'package:meta/meta.dart';
import 'package:easychat/bloc/profession/bloc.dart';

@immutable
class ProfessionState {
  final bool isProfessionEmpty;
  final bool isFailure;
  final bool isSubmitting;
  final bool isSuccess;

  bool get isFormValid => isProfessionEmpty;
  ProfessionState(
      {@required this.isProfessionEmpty,
      @required this.isFailure,
      @required this.isSubmitting,
      @required this.isSuccess});
  factory ProfessionState.empty() {
    return ProfessionState(
        isProfessionEmpty: false,
        isFailure: false,
        isSubmitting: false,
        isSuccess: false);
  }
  factory ProfessionState.loading() {
    return ProfessionState(
        isProfessionEmpty: false,
        isFailure: false,
        isSubmitting: true,
        isSuccess: false);
  }
  factory ProfessionState.failure() {
    return ProfessionState(
        isProfessionEmpty: false,
        isFailure: true,
        isSubmitting: false,
        isSuccess: false);
  }
  factory ProfessionState.success() {
    return ProfessionState(
        isProfessionEmpty: false,
        isFailure: false,
        isSubmitting: false,
        isSuccess: true);
  }

  ProfessionState update({bool isProfessionEmpty}) {
    return copyWith(
      isProfessionEmpty: isProfessionEmpty,
      isFailure: false,
      isSuccess: false,
      isSubmitting: false,
    );
  }

  ProfessionState copyWith({
    bool isProfessionEmpty,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
  }) {
    return ProfessionState(
        isProfessionEmpty: isProfessionEmpty ?? this.isProfessionEmpty,
        isFailure: isFailure ?? this.isFailure,
        isSubmitting: isSubmitting ?? this.isSubmitting,
        isSuccess: isSuccess ?? this.isSuccess);
  }
}
