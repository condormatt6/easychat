import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:easychat/repository/userRepository.dart';
import 'package:flutter/cupertino.dart';
import './bloc.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository _userRepository;

  AuthenticationBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  AuthenticationState get initialState => Uninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      yield* _mapAppStartedToState();
    } else if (event is LoggedIn) {
      yield* _mapLoggedInToState();
    } else if (event is LoggedOut) {
      yield* _mapLoggedOutToState();
    }
  }

  Stream<AuthenticationState> _mapAppStartedToState() async* {
    try {
      final isSignedIn = await _userRepository.isSignedIn();
      if (isSignedIn) {
        final uid = await _userRepository.getUser();
        final isFirstTime = await _userRepository.isFirstTime(uid);
        final isLocation = await _userRepository.isLocationSet(uid);
        final isProfession = await _userRepository.isProfessionSet(uid);

        if (!isFirstTime) {
          yield AuthenticatedButNotSet(uid);
        } else if (isLocation) {
          yield Authenticated(uid);
        } else {
          if (!isProfession) {
            yield AuthenticatedButSetNameAndPhoto(uid);
          } else if (isProfession) {
            yield AuthenticatedButSetProfession(uid);
          } else {
            yield Authenticated(uid);
          }
        }
      } else {
        yield Unauthenticated();
      }
    } catch (_) {
      yield Unauthenticated();
    }
  }

  Stream<AuthenticationState> _mapLoggedInToState() async* {
    final uid = await _userRepository.getUser();
    final isFirstTime = await _userRepository.isFirstTime(uid);

    final isSignedIn = await _userRepository.isSignedIn();
    if (!isFirstTime) {
      yield AuthenticatedButNotSet(uid);
    } else if (isSignedIn) {
      final isImageSet = await _userRepository.isImageAndNameSet(uid);
      final isProfession = await _userRepository.isProfessionSet(uid);
      final isLocation = await _userRepository.isLocationSet(uid);

      if (!isFirstTime) {
        yield AuthenticatedButNotSet(uid);
      } else if (isLocation) {
        yield Authenticated(uid);
      } else {
        if (isProfession) {
          yield AuthenticatedButSetProfession(uid);
        } else if (isImageSet) {
          yield AuthenticatedButSetNameAndPhoto(uid);
        }
      }
    } else {
      yield Unauthenticated();
    }
  }

  Stream<AuthenticationState> _mapLoggedOutToState() async* {
    yield Unauthenticated();
    _userRepository.signOut();
  }
}
