import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:geolocator/geolocator.dart';

class UserRepository {
  final FirebaseAuth _firebaseAuth;
  final FirebaseFirestore _firestore;

  UserRepository({FirebaseAuth firebaseAuth, FirebaseFirestore firestore})
      : _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance,
        _firestore = firestore ?? FirebaseFirestore.instance;

  Future<void> signInWithEmail(String email, String password) {
    return _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
  }

  Future<bool> isFirstTime(String userId) async {
    bool exist;
    await FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .get()
        .then((user) {
      exist = user.exists;
    });
    return exist;
  }

  Future<void> signUpWithEmail(String email, String password) async {
    return await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
  }

  Future<void> signOut() async {
    return await _firebaseAuth.signOut();
  }

//check if user is signed
  Future<bool> isSignedIn() async {
    final currentUser = _firebaseAuth.currentUser;
    return currentUser != null;
  }

  Future<String> getUser() async {
    return (await _firebaseAuth.currentUser.uid);
  }

//Get userName
  Future<String> getUserName() async {
    String name;
    var firebaseUser;
    final firestoreInstance = FirebaseFirestore.instance;

    firebaseUser = FirebaseAuth.instance.currentUser;
    firestoreInstance
        .collection("users")
        .doc(firebaseUser.uid)
        .get()
        .then((value) {
      print(value.data()['name']);
      name = value.data()['name'];
    });
    return name;
  }

//check if Name is setted
  Future<bool> isImageAndNameSet(String userId) async {
    bool exist;
    await FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .get()
        .then((value) {
      exist = value.data().containsKey('name');
    });
    return exist;
  }

// check if Profession is setted
  Future<bool> isProfessionSet(String userId) async {
    bool exist;
    await FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .get()
        .then((value) {
      exist = value.data().containsKey('profession');
    });
    return exist;
  }

// check if localisation is setted
  Future<bool> isLocationSet(String userId) async {
    bool exist;
    await FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .get()
        .then((value) {
      exist = value.data().containsKey('location');
    });
    return exist;
  }

//Get localisation
  Future<Position> getLocalisation({String userId}) async {
    Position position;
    await FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .get()
        .then((value) {
      position = value.data()['location'];
    });
    return position;
  }

//Image,name and userId setup
  Future<void> photoAndImageSetup(
      File photo, String name, String userId) async {
    UploadTask storageUploadTask;
    storageUploadTask = FirebaseStorage.instance
        .ref()
        .child('userphotos')
        .child(userId)
        .child(userId)
        .putFile(photo);
    return await storageUploadTask.then((ref) async {
      await ref.ref.getDownloadURL().then((url) async {
        await _firestore.collection('users').doc(userId).set({
          'uid': userId,
          'photoUrl': url,
          'name': name,
        });
      });
    });
  }
  //profession setup

  Future<void> setupProfession(String profession, userId) async {
    return await FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .set({'profession': profession}, SetOptions(merge: true));
  }

  //profile setup
  Future<void> profileSetup(
      {String gender,
      String interestedIn,
      DateTime age,
      GeoPoint location,
      userId}) async {
    return await FirebaseFirestore.instance.collection('users').doc(userId).set(
        {
          'gender': gender,
          'interestedIn': interestedIn,
          'age': age,
          'location': location
        },
        SetOptions(merge: true));
  }
  //set token to user for cloud messaging
}
