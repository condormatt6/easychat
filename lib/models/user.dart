import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class User {
  String name,
      profession,
      hobbies,
      photo,
      uid,
      gender,
      subscription,
      interestedIn;
  Timestamp age;
  GeoPoint location;

  User(
      {this.name,
      this.location,
      this.hobbies,
      this.photo,
      this.gender,
      this.profession,
      this.age,
      this.interestedIn,
      this.subscription,
      this.uid});
}
